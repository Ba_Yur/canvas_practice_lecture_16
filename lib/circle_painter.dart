
import 'package:canvas_practice_lecture_16/size_const.dart';
import "package:flutter/material.dart";

import 'dart:math';
import 'dart:ui' as ui;

const blue_color = Colors.blue;
const purple_color = Colors.purple;
const Black_color = Colors.black;
const red_color = Colors.red;
const green_color = Colors.green;
const yellow_color = Colors.yellow;
const List<Point> POINT = [Point(100, 100)];

SizeUtil get _sizeUtil {
  return SizeUtil.getInstance(key: SizeKeyConst.CIRCLE_KEY);
}

class CirclePainter extends CustomPainter {
  CirclePainter({this.startAngle});

  final double startAngle;

  @override
  void paint(ui.Canvas canvas, ui.Size size) {
    //580*648
    if (size.width > 1.0 && size.height > 1.0) {
      print(">1.9");
      _sizeUtil.logicSize = size;
    }
    var paint = Paint()
      ..style = PaintingStyle.fill
      ..color = blue_color
      ..strokeWidth = 2.0
      ..isAntiAlias = true;
    paint.color = Colors.grey[900];
//    canvas.drawCircle(
//        Offset(_sizeUtil.getAxisX(250), _sizeUtil.getAxisY(250.0)),
//        _sizeUtil.getAxisBoth(200.0),
//        paint);
    paint.color = green_color;
    paint.strokeWidth = 20;
    paint.style = PaintingStyle.stroke;
    var center = Offset(
      _sizeUtil.getAxisX(250.0),
      _sizeUtil.getAxisY(250.0),
    );
    var radius = _sizeUtil.getAxisBoth(200);
    _drawArcGroup(
      canvas,
      center: center,
      radius: radius,
      sources: [1, 2, 1, 2, 1, 2, 1, 4, 1, 2],
      colors: [red_color, green_color, Black_color, purple_color, yellow_color],
      paintWidth: 10.0,
      startAngle: 0.0,
      hasEnd: false,
      hasCurrent: true,
      curPaintWidth: 30.0,
      curIndex: 7,
    );
    canvas.save();
    canvas.restore();
  }


  void _drawArcGroup(Canvas canvas,
      {Offset center,
        double radius,
        List<double> sources,
        List<Color> colors,
        double startAngle = 0.0,
        double paintWidth = 10.0,
        bool hasEnd = false,
        hasCurrent = false,
        int curIndex = 0,
        curPaintWidth = 12.0}) {
    assert(sources != null && sources.length > 0);
    assert(colors != null && colors.length > 0);
    var paint = Paint()
      ..style = PaintingStyle.fill
      ..color = blue_color
      ..strokeWidth = paintWidth
      ..isAntiAlias = true;
    double total = 0;
    for (double d in sources) {
      total += d;
    }
    assert(total > 0.0);
    List<double> radians = List<double>();
    for (double d in sources) {
      double radian = d * 2 * pi / total;
      radians.add(radian);
    }
    var startA = startAngle;
    paint.style = PaintingStyle.stroke;
    var curStartAngle = 0.0;
    for (int i = 0; i < radians.length; i++) {
      var rd = radians[i];
      if (hasCurrent && curIndex == i) {
        curStartAngle = startA;
        startA += rd;
        continue;
      }
      paint.color = colors[i % colors.length];
      paint.strokeWidth = paintWidth;
      _drawArcWithCenter(canvas, paint,
          center: center, radius: radius, startRadian: startA, sweepRadian: rd);
      startA += rd;
    }
    if (hasEnd) {
      startA = startAngle;
      paint.strokeWidth = paintWidth;
      for (int i = 0; i < radians.length; i++) {
        var rd = radians[i];
        if (hasCurrent && curIndex == i) {
          startA += rd;
          continue;
        }
        paint.color = colors[i % colors.length];
        paint.strokeWidth = paintWidth;
        _drawArcTwoPoint(canvas, paint,
            center: center,
            radius: radius,
            startRadian: startA,
            sweepRadian: rd,
            hasEndArc: true);
        startA += rd;
      }
    }

    if (hasCurrent) {
      paint.color = colors[curIndex % colors.length];
      paint.strokeWidth = curPaintWidth;
      paint.style = PaintingStyle.stroke;
      _drawArcWithCenter(canvas, paint,
          center: center,
          radius: radius,
          startRadian: curStartAngle,
          sweepRadian: radians[curIndex]);
    }
    if (hasCurrent && hasEnd) {
      var rd = radians[curIndex % radians.length];
      paint.color = colors[curIndex % colors.length];
      paint.strokeWidth = curPaintWidth;
      paint.style = PaintingStyle.fill;
      _drawArcTwoPoint(canvas, paint,
          center: center,
          radius: radius,
          startRadian: curStartAngle,
          sweepRadian: rd,
          hasEndArc: true,
          hasStartArc: true);
    }
  }

  void _drawArcWithCenter(
      Canvas canvas,
      Paint paint, {
        Offset center,
        double radius,
        startRadian = 0.0,
        sweepRadian = pi,
      }) {
    canvas.drawArc(
      Rect.fromCircle(center: center, radius: radius),
      startRadian,
      sweepRadian,
      false,
      paint,
    );
  }

  void _drawArcTwoPoint(Canvas canvas, Paint paint,
      {Offset center,
        double radius,
        startRadian = 0.0,
        sweepRadian = pi,
        hasStartArc = false,
        hasEndArc = false}) {
    var smallR = paint.strokeWidth / 2;
    paint.strokeWidth = smallR;
    if (hasStartArc) {
      var startCenter = LineCircle.radianPoint(
          Point(center.dx, center.dy), radius, startRadian);
      paint.style = PaintingStyle.fill;
      canvas.drawCircle(Offset(startCenter.x, startCenter.y), smallR, paint);
    }
    if (hasEndArc) {
      var endCenter = LineCircle.radianPoint(
          Point(center.dx, center.dy), radius, startRadian + sweepRadian);
      paint.style = PaintingStyle.fill;
      canvas.drawCircle(Offset(endCenter.x, endCenter.y), smallR, paint);
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => true;
}

class LineCircle {
  static Point radianPoint(Point center, double r, double radian) {
    return Point(center.x + r * cos(radian), center.y + r * sin(radian));
  }
}
