import 'dart:math';

import 'package:flutter/material.dart';
import 'Circle_page.dart';
import 'circle_painter.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(

      home: CirclePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  const MyHomePage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Canvas practice'),
      ),
      body: Center(
        child: Container(
          height: 580,
          width: 648,
          child: CustomPaint(
            painter: CirclePainter(startAngle: 5),
            child: Container(),
          ),
        ),
      ),
    );
  }
}


