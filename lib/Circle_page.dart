import 'package:canvas_practice_lecture_16/size_const.dart';
import 'package:flutter/material.dart';

import 'circle_painter.dart';

class CirclePage extends StatefulWidget {
  @override
  _CircleState createState() => _CircleState();
}

class _CircleState extends State<CirclePage> {

  @override
  Widget build(BuildContext context) {
    SizeUtil.getInstance().logicSize = MediaQuery.of(context).size;
    SizeUtil.initDesignSize();
    return Scaffold(
      appBar: AppBar(
        title: Text('Canvas practice'),
      ),
      body: Container(
          child: Center(
              child: Container(
                width: 300,
                height: 300,
                child: CustomPaint(
                  painter: CirclePainter(startAngle: 0),
                  child: Container(),
                ),
              ))),
    );
  }
}